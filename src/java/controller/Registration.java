
package controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import static jdk.nashorn.internal.objects.NativeError.printStackTrace;
import model.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.AnnotationConfiguration;


@WebServlet(name = "Registration", urlPatterns = {"/Registration"})
public class Registration extends HttpServlet {

    private static SessionFactory sessionFactory = new AnnotationConfiguration().configure().buildSessionFactory();
    private static Session session;
    private static Transaction transaction;
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       
    }

 
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String name = request.getParameter("name");
        String email = request.getParameter("email");
        String mobile = request.getParameter("mobile");
        String userAge = request.getParameter("age");
        Integer age = Integer.parseInt(userAge);
        String password = request.getParameter("password");
        String image = request.getParameter("image");
        
        System.out.println("Name : "+name+" E-mail : "+email+" Mobile : "+mobile+" Age : "+age+" Password : "+password+" Image : "+image);
        
        User user = new User();
        user.setAge(age);
        user.setEmail(email);
        user.setImage(image);
        user.setName(name);
        user.setMobile(mobile);
        user.setPassword(password);
        
        session = sessionFactory.openSession();
        transaction = session.beginTransaction();
        int i=0;
        try {
            System.out.println("Try clause visited");
            session.save(user);
            transaction.commit();
            i=1;
        } catch (Exception e) {
            System.out.println("Catch clause visited");
//            if(transaction!=null){
//                transaction.rollback();
//            }
String errorMessage = "Sorry , Something went wrong. Please try again ..";
            request.setAttribute("errorMessage", errorMessage);
            request.getRequestDispatcher("signUp.jsp").forward(request, response);
            printStackTrace(e);
        }finally{
            
            
            
            session.close();
        }
        sessionFactory.close();
        
        
        if(i==1){
            String successMessage = "Successfully registered.";
            request.setAttribute("successMessage", successMessage);
            request.getRequestDispatcher("index.jsp").forward(request, response);
        }else{
            String errorMessage = "Sorry , Something went wrong. Please try again ..";
            request.setAttribute("errorMessage", errorMessage);
            request.getRequestDispatcher("signUp.jsp").forward(request, response);
        }
    }

 
}
