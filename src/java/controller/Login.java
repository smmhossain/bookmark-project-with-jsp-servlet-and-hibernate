
package controller;

import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.User;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.AnnotationConfiguration;


@WebServlet(name = "Login", urlPatterns = {"/Login"})
public class Login extends HttpServlet {


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String email = request.getParameter("email");
        String password = request.getParameter("password");
     
        System.out.println("E-mail : "+email+" Password : "+password);
        
        SessionFactory sessionFactory = new AnnotationConfiguration().configure().buildSessionFactory();
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();;
        Query query;
        String hql;
        List<User> users;
        
        
        int flag=1;
        
        
        User user;
        
        try {
            
            
            System.out.println("Try clause visited");
              
            user = (User) session.createQuery("FROM User U WHERE U.email=:e").setParameter("e", email).uniqueResult();
            
            if(user==null){
                flag=0;
            }else{
        
                String userPassword = user.getPassword();
                System.out.println("Password : "+userPassword);
                if(password.equals(userPassword)){
                    
                    HttpSession httpSession = request.getSession();
                    
                Integer userId = user.getId();
                Integer userAge = user.getAge();
                String userName = user.getName();
                String userEmail = user.getEmail();
                String userMobile = user.getMobile();
                String userImage = user.getImage();
                System.out.println(userId+userAge+userName+userEmail+userMobile);
                
                httpSession.setAttribute("userId", userId);
                httpSession.setAttribute("userAge", userAge);
                httpSession.setAttribute("userName", userName);
                httpSession.setAttribute("userEmail", userEmail);
                httpSession.setAttribute("userMobile", userMobile);
                httpSession.setAttribute("userImage", userImage);
                
                
                flag=1;
                }else{
                    flag=0;
                }
            }
            
            transaction.commit();
            
        } catch (Exception e) {
            System.out.println("Catch clause visited");
            if(transaction!=null){
                transaction.rollback();
            }
            System.out.println(e);
            flag=0;
        }finally{
            session.close();
        }
        sessionFactory.close();
        
        if(flag==1){
            request.getRequestDispatcher("home.jsp").forward(request, response);
        }else if(flag==0){
            String msg = "Wrong E-mail or password";
                    request.setAttribute("msg", msg);
                    request.getRequestDispatcher("index.jsp").forward(request, response);
        }
        
    }

}
