
package controller;

import com.sun.javafx.scene.text.TextLayout;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javaCode.BookmarkInfos;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.BookmarkTable;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;


@WebServlet(name = "Bookmark", urlPatterns = {"/Bookmark"})
public class Bookmark extends HttpServlet {


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
    }

  
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
     String link = request.getParameter("link");
     String details = request.getParameter("details");
     String comment = request.getParameter("comment");
     HttpSession httpSession = request.getSession();
     Integer userId =(Integer) httpSession.getAttribute("userId");
     Integer status = 1;
        System.out.println("UserId : "+userId+" Link : "+link+" Details : "+details+" Comment : "+comment);
        
        
        BookmarkTable b = new BookmarkTable();
        b.setUserId(userId);
        b.setComment(comment);
        b.setLink(link);
        b.setDetails(details);
        b.setStatus(status);
        SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        
        int flag;
        try {
            System.out.println("Try clause visited");
            session.save(b);
            transaction.commit();
            flag = 1;
        } catch (Exception e) {
            System.out.println("Catch clause visited");
            if(transaction!=null){
                transaction.rollback();
            }
            e.printStackTrace();
            flag = 0;
            
        }finally{
            session.close();
        }
        sessionFactory.close();
        
        if(flag==1){
            
            List<BookmarkTable> infos = BookmarkInfos.getBookmarkInfos(userId);
            request.setAttribute("infos", infos);
            String msg = "Successfully bookmarked";
            request.setAttribute("msg", msg);
            request.getRequestDispatcher("bookmark.jsp").forward(request, response);
        }else if(flag==0){
             String msg = "Sorry, Please try again";
            request.setAttribute("msg", msg);
            request.getRequestDispatcher("bookmark.jsp").forward(request, response);
        }
        
    }


}
