package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javaCode.BookmarkInfos;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.BookmarkTable;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

@WebServlet(name = "BookmarkController", urlPatterns = {"/BookmarkController"})
public class BookmarkController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        System.out.println("Bookamrk controller visited");
        HttpSession httpSession = request.getSession();
        Integer userId = (Integer) httpSession.getAttribute("userId");
        System.out.println("UserId : "+userId);
        
        List<BookmarkTable> infos = BookmarkInfos.getBookmarkInfos(userId);

        for(BookmarkTable bt: infos){
            System.out.println(bt.getId());
            System.out.println(bt.getUserId());
            System.out.println(bt.getLink());
            System.out.println(bt.getComment());
            System.out.println(bt.getDetails());
        }
        
        request.setAttribute("infos", infos);
        request.getRequestDispatcher("bookmark.jsp").forward(request, response);
    }
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
    }

}
