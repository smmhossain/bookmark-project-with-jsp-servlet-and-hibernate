package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javaCode.ContactInfomations;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.ContactTable;

@WebServlet(name = "ContactController", urlPatterns = {"/ContactController"})
public class ContactController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        System.out.println("ContactController class doGet method visited");
        
        HttpSession httpSession = request.getSession();
        Integer userId =(Integer) httpSession.getAttribute("userId");
        List<ContactTable> infos = new ArrayList<ContactTable>();
        infos = ContactInfomations.getContactInfos(userId);
        
        for(ContactTable ct :infos){
            System.out.println(ct.getId());
            System.out.println(ct.getName());
            System.out.println(ct.getUserId());
            System.out.println(ct.getEmail());
            System.out.println(ct.getMobile());
            System.out.println(ct.getAddress());
        }
        request.setAttribute("contactInfos", infos);
        request.getRequestDispatcher("contact.jsp").forward(request, response);
    }
 @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
    }


}
