package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javaCode.ContactInfomations;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.ContactTable;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

@WebServlet(name = "AddContact", urlPatterns = {"/AddContact"})
public class AddContact extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
    }
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        System.out.println("AddContact Controller doPost method visited");
        HttpSession httpSession = request.getSession();
        Integer userId =(Integer) httpSession.getAttribute("userId");
        String name = request.getParameter("name");
        String email = request.getParameter("email");
        String mobile = request.getParameter("mobile");
        String address = request.getParameter("address");
        
        System.out.println("UserId : "+userId+" Name  : "+name+" E-mail : "+email+" Mobile : "+mobile+" Address : "+address);
    
        SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        
        ContactTable ct = new ContactTable();
        ct.setAddress(address);
        ct.setEmail(email);
        ct.setName(name);
        ct.setUserId(userId);
        ct.setMobile(mobile);
        Integer flag;
        try {
            
            session.save(ct);
            System.out.println("Try clause visited");
            transaction.commit();
            flag = 1;
            
        } catch (Exception e) {
            System.out.println("Catch Clause visited");
            if(transaction!=null){
                transaction.rollback();
            }
            e.printStackTrace();
            flag=0;           
        }finally{
            session.close();
        }
        sessionFactory.close();
        
        if(flag==1){
            
            List<ContactTable> infos = ContactInfomations.getContactInfos(userId);
            request.setAttribute("contactInfos", infos);
            String msg = "Data successfully added";
            request.setAttribute("msg", msg);
            request.getRequestDispatcher("contact.jsp").forward(request, response);
        }else if(flag==0){
            String msg = "Something went wrong. Please try again...";
            request.setAttribute("msg", msg);
            request.getRequestDispatcher("contact.jsp").forward(request, response);
        }
    }

}
