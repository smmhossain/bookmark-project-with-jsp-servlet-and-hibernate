//
//package javaCode;
//
//
//import model.User;
//
//import org.hibernate.SessionFactory;
//import org.hibernate.Session;
//import org.hibernate.Transaction;
//import org.hibernate.cfg.AnnotationConfiguration;
//
//
//
//public class TestClass {
//    
//    
//    private static SessionFactory sessionFactory = new AnnotationConfiguration().configure().buildSessionFactory();
//    private static Session session;
//    private static Transaction transaction;
//        
//    public static void main(String[] args) {
//        System.out.println("Main method visited");
//        
//        User user = new User();
//        user.setAge(20);
//        user.setEmail("s@gmail.com");
//        user.setName("Samim");
//        user.setPassword("123");
//        user.setMobile("123445566");
//        user.setImage("image.jpg");
//        
//        session = sessionFactory.openSession();
//        transaction = session.beginTransaction();
//        
//        try {
//            System.out.println("Try clause visited");
//            session.save(user);
//            transaction.commit();
//            
//            
//            
//        } catch (Exception e) {
//            System.out.println("Catch clause visited");
//            if(transaction!=null){
//                transaction.rollback();
//            }
//        }finally{
//            System.out.println("Finally clause visited");
//            session.close();
//        }
//     sessionFactory.close();
//        
//        
//
//    }
//}
