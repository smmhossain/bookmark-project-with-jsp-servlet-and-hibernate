/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaCode;

import com.sun.xml.internal.bind.unmarshaller.InfosetScanner;
import java.util.List;
import model.ContactTable;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;


public class ContactInfomations {
    

        public static List<ContactTable> getContactInfos(Integer userId){
            SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
            Session session = sessionFactory.openSession();
            Transaction transaction = session.beginTransaction();

            String hql = "FROM ContactTable CT where CT.userId =:uid";
            Query query = session.createQuery(hql);
            query.setParameter("uid",userId );

            List<ContactTable> contactInfos = query.list();

            return contactInfos;
        
        }
    }

