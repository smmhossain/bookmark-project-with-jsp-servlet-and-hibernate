/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaCode;

import java.util.List;
import model.BookmarkTable;
import model.ContactTable;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

/**
 *
 * @author User
 */
public class BookmarkInfos {
    
            public static List<BookmarkTable> getBookmarkInfos(Integer userId){
            SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
            Session session = sessionFactory.openSession();
            Transaction transaction = session.beginTransaction();

            String hql = "FROM BookmarkTable b where b.userId =:uid";
            Query query = session.createQuery(hql);
            query.setParameter("uid",userId );

            List<BookmarkTable> bookmarkTables = query.list();

            return bookmarkTables;
        
        }
}
